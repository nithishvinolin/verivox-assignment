package pages.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.locators.VerivoxHomePageLocators;
import utils.SeleniumDriver;

public class VerivoxHomePageActions {
	
	VerivoxHomePageLocators verivoxHomePageLocators=null;
	public WebDriverWait wait;
	public VerivoxHomePageActions()
	{
		
		this.verivoxHomePageLocators=new VerivoxHomePageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), verivoxHomePageLocators);
	}
	
	public void clickPrivacy() {
		verivoxHomePageLocators.privacy.click();
	}
	public void clickDSL()
	{
		verivoxHomePageLocators.DSL_Link.click();
	}
	public void enterValueToIhreVorwahl(String Vorwahl)
	{
		wait = new WebDriverWait(SeleniumDriver.getDriver(), 10);
		wait.until(ExpectedConditions.elementToBeClickable(verivoxHomePageLocators.IhreVorwahl_INPUT));
		
		verivoxHomePageLocators.IhreVorwahl_INPUT.click();
		verivoxHomePageLocators.IhreVorwahl_INPUT.clear();
		verivoxHomePageLocators.IhreVorwahl_INPUT.sendKeys(Vorwahl);
	}
	
	public void selectGeschwindigkeit()
	{
		verivoxHomePageLocators.Geschwindigkeit_LINK.click();
		
	}
	
	public void clickJetztVergleichen()
	{
		verivoxHomePageLocators.JETZTVERGLEICHEN_BUTTON.click();
		
	}

}
