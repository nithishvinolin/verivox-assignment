package pages.actions;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pages.locators.VerivoxSearchDetailsPageLocators;
import pages.locators.VerivoxSearchPageLocators;
import utils.SeleniumDriver;
import pages.actions.VerivoxDetailsPageActions;

public class VerivoxSearchPageActions {
	public WebDriverWait wait;
	VerivoxSearchPageLocators verivoxSearchPageLocators=null;
	VerivoxSearchDetailsPageLocators verivoxSearchDetailsPageLocators=new VerivoxSearchDetailsPageLocators();
	public String billingValText;
	public String currentUrl;
	public String providerNmText;
	public String vndrNmText;
	
	public  VerivoxSearchPageActions()
	{
		this.verivoxSearchPageLocators= new VerivoxSearchPageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), verivoxSearchPageLocators);
	}

	public int getResultCount()
	{
		
		int actualSize = verivoxSearchPageLocators.vndrNMs.size();
		
		return actualSize;
	}
	public int getExpectedCount()
	{
		String countStr = verivoxSearchPageLocators.resultCount.getText().replaceAll("[a-zA-Z ]", "");
		int expectedSize = Integer.parseInt(countStr);
		
		return expectedSize;
		
	}
	
	public int getCount()
	{
		
		int actualSize = verivoxSearchPageLocators.searchItems.size();
		
		return actualSize;
	}
	
	
	public void tarifBUttonCheck() {
		try {
			do
			{
				try {
					verivoxSearchPageLocators.tarriffButton.click();
					wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class,'btn btn-primary')]")));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			while(verivoxSearchPageLocators.tarriffButton.isDisplayed());
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		System.out.println("Actual search result size is "+getResultCount());
		System.out.println("Expected search result size is "+getExpectedCount());
		
try {
	Assert.assertEquals(getResultCount(), getExpectedCount());
} catch (Exception e) {
	e.printStackTrace();
}		
if (getResultCount()==getExpectedCount()) {
System.out.println("SUCCESS - Total Search result count matched the total number of Search result displayed");
}
else {
	System.out.println("SUCCESS - Total Search result count DID NOT matched the total number of Search result displayed");
}
	}
	
	public void verifyTitle(String expectedPageTitle) {
		String ActualPageTitle= SeleniumDriver.getDriver().getTitle();
		  System.out.println("Actual page title-->"+ActualPageTitle);
		  System.out.println("Expected page title-->"+expectedPageTitle);
		  Assert.assertEquals(expectedPageTitle, ActualPageTitle);
		  
	}
	
	public boolean  weitere_tarife_laden_Exists() {
		if(verivoxSearchPageLocators.loadMoreBtn.isDisplayed()) {
			return true;
		}
		return false;
		
	}
	public void  buttonCheck(String buttonText)
	{
		
		wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);
		wait.until(ExpectedConditions.elementToBeClickable(verivoxSearchPageLocators.loadMoreBtn));
		
		Assert.assertEquals(verivoxSearchPageLocators.loadMoreBtn.getText(),buttonText);
		if(verivoxSearchPageLocators.loadMoreBtn.getText().equals(buttonText) ) {
	    	System.out.println("The expected button name "+buttonText+" MATCHED the actual Button name in screen "+verivoxSearchPageLocators.loadMoreBtn.getText());
	    }
		else {
			System.out.println("The expected button name "+buttonText+" DID NOT MATCHED the actual Button name in screen "+verivoxSearchPageLocators.loadMoreBtn.getText());
		}
	    
	}
	
	public void click_WEITERE_TARIFE_LADEN() {
		verivoxSearchPageLocators.loadMoreBtn.click();
	}
	
	
	public void click_ZUM_ANGEBOT(int i) {
		verivoxSearchPageLocators.zumAngebotBtns.get(i).click();
		wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@class='price'])[1]")));
	}
	
	public void getTariffDetails(int i) {
		 billingValText=verivoxSearchPageLocators.billingVal.get(i).getText();
		 
		 providerNmText = verivoxSearchPageLocators.providerNM.get(i).getText();
		 vndrNmText = verivoxSearchPageLocators.vndrNM.get(i).getText();
		 currentUrl = SeleniumDriver.getDriver().getCurrentUrl();
		
	}
}
