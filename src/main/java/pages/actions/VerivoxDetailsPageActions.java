package pages.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pages.locators.VerivoxHomePageLocators;
import pages.locators.VerivoxSearchDetailsPageLocators;
import pages.locators.VerivoxSearchPageLocators;
import utils.SeleniumDriver;

public class VerivoxDetailsPageActions {
	
	VerivoxHomePageLocators verivoxHomePageLocators=null;
	VerivoxSearchDetailsPageLocators verivoxSearchDetailsPageLocators=null;
	VerivoxSearchPageLocators verivoxSearchPageLocators=new VerivoxSearchPageLocators();
	VerivoxSearchPageActions verivoxSearchPageActions=new VerivoxSearchPageActions();
	VerivoxHomePageActions verivoxHomePageActions=new VerivoxHomePageActions();
	
	public WebDriverWait wait;
	public VerivoxDetailsPageActions()
	{
		
		this.verivoxSearchDetailsPageLocators=new VerivoxSearchDetailsPageLocators();
		PageFactory.initElements(SeleniumDriver.getDriver(), verivoxSearchDetailsPageLocators);
	}
	
	public void verifyPrice(String price) {
		Assert.assertEquals(verivoxSearchDetailsPageLocators.price.get(0).getText(), price);
		Assert.assertEquals(verivoxSearchDetailsPageLocators.price.get(1).getText(), price);
		
		if(verivoxSearchDetailsPageLocators.price.get(0).getText().equals(price))
		{
			System.out.println("Tariff amount in Search results screen "+price+ " MATCHED the tariff amount in details screen page which is situated at top of the screen "+verivoxSearchDetailsPageLocators.price.get(0));
		}
		else
		{
			System.out.println("Tariff amount in Search results screen "+price+ " DID NOT MATCHED the tariff amount in details screen page which is situated at top of the screen "+verivoxSearchDetailsPageLocators.price.get(0));
		}
		
		if(verivoxSearchDetailsPageLocators.price.get(1).getText().equals(price))
		{
			System.out.println("Tariff amount in Search results screen "+price+ " MATCHED the tariff amount in details screen page which is situated at bottom of the screen "+verivoxSearchDetailsPageLocators.price.get(1));
		}
		else
		{
			System.out.println("Tariff amount in Search results screen "+price+ " DID NOT MATCHED the tariff amount in details screen page which is situated at bottom of the screen "+verivoxSearchDetailsPageLocators.price.get(1));
		}
	}
	
	public void verifyProviderNM(String providerNM) {
		Assert.assertEquals(verivoxSearchDetailsPageLocators.providerNm_detail.getText(), providerNM);
		
		if(verivoxSearchDetailsPageLocators.providerNm_detail.getText().equals(providerNM))
		{
			System.out.println("Provider Name in Search results screen "+providerNM+ " MATCHED the Provider Name in details screen page "+verivoxSearchDetailsPageLocators.providerNm_detail.getText());
		}
		else
		{
			System.out.println("Tariff amount in Search results screen "+providerNM+ " DID NOT MATCHED the tariff amount in details screen page which is situated at top of the screen "+verivoxSearchDetailsPageLocators.providerNm_detail.getText());
		}
	}
	
	public void verifyVndrNM(String vndrNM) {
		Assert.assertEquals(verivoxSearchDetailsPageLocators.vndrNm_detail.getText(), vndrNM);
		
		if(verivoxSearchDetailsPageLocators.vndrNm_detail.getText().equals(vndrNM))
		{
			System.out.println("Provider Name in Search results screen "+vndrNM+ " MATCHED the Provider Name in details screen page "+verivoxSearchDetailsPageLocators.vndrNm_detail.getText());
		}
		else
		{
			System.out.println("Tariff amount in Search results screen "+vndrNM+ " DID NOT MATCHED the tariff amount in details screen page which is situated at top of the screen "+verivoxSearchDetailsPageLocators.vndrNm_detail.getText());
		}
	}
	
	public void verify5MinOnlineWeschseln() {
		Assert.assertEquals(verivoxSearchDetailsPageLocators.onlineWechseln.isDisplayed(), true);
		try {
			if(verivoxSearchDetailsPageLocators.onlineWechseln.isDisplayed()) {
				System.out.println("In 5 Minuten online wechseln button exists in the Tariff Details screen");
			}
		} catch (Exception e) {
			System.out.println("In 5 Minuten online wechseln button DOES NOT exists in the Tariff Details screen");
		}
	}
	
	public void verifyJetxtWechseln() {
		Assert.assertEquals(verivoxSearchDetailsPageLocators.jetxtWechseln.isDisplayed(), true);
		try {
			if(verivoxSearchDetailsPageLocators.jetxtWechseln.isDisplayed()) {
				System.out.println("Jetzt wechseln button exists in the Tariff Details screen");
			}
		} catch (Exception e) {
			System.out.println("Jetzt wechseln button DOES NOT exists in the Tariff Details screen");
		}
	}
	
	public void click_zurickButton() {
		verivoxSearchDetailsPageLocators.zuruckBtn.click();
		
		wait = new WebDriverWait(SeleniumDriver.getDriver(), 30);
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(@class,'pt-xl-4 pt-2')]")));
		} catch (Exception e) {
			verivoxHomePageActions.clickDSL();
			verivoxHomePageActions.enterValueToIhreVorwahl("030");
			verivoxHomePageActions.selectGeschwindigkeit();
			verivoxHomePageActions.clickJetztVergleichen();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(@class,'pt-xl-4 pt-2')]")));
		} 
	}
	 

}
