package pages.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class VerivoxHomePageLocators {
	
	@FindBy(how=How.CLASS_NAME,using="gdpr-vx-consent-bar-button")
	public WebElement privacy;
	
	@FindBy(how=How.XPATH,using="//span[text()='DSL']")
	public WebElement DSL_Link;
	
	@FindBy(how=How.XPATH,using="(//input[@name='Prefix'])[1]")
	public WebElement IhreVorwahl_INPUT;
	
	@FindBy(how=How.XPATH,using="(//strong[text()='16 '])[1]")
	public WebElement Geschwindigkeit_LINK ;
	
	@FindBy(how=How.XPATH,using="(//button[text()='Jetzt vergleichen'])[5]")
	public WebElement JETZTVERGLEICHEN_BUTTON;	
	
	

}
