package pages.locators;


import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class VerivoxSearchPageLocators {
	
	
	@FindBy(how=How.CLASS_NAME,using="summary-tariff")
	public WebElement resultCount;
	
	@FindBy(how=How.XPATH,using="//button[contains(@class,'btn btn-primary')]")
	public WebElement tarriffButton;
	
	@FindAll(@FindBy(how=How.XPATH,using="//div[contains(@class, 'row align-items-center ')]"))
	public List<WebElement> searchItems;
	
	@FindBy(how=How.XPATH,using="//div[@class='col-12 text-center']//button[1]")
	public WebElement loadMoreBtn;
	
	@FindAll(@FindBy(how=How.XPATH,using="//a[contains(text(),'Zum Angebot')]"))
	public List<WebElement> zumAngebotBtns;
	
	@FindAll(@FindBy(how=How.XPATH,using="//a[contains(text(),'Zum Angebot')]/ancestor::div[5]//strong[@class='headline-large']"))
	public List<WebElement> providerNM;
	
	@FindAll(@FindBy(how=How.XPATH,using="//a[contains(text(),'Zum Angebot')]/ancestor::div[5]//div[contains(@class,'headline-short-name')]"))
	public List<WebElement> vndrNM;
	
	@FindAll(@FindBy(how=How.XPATH,using="(//div[contains(@class,'headline-short-name px-sm-2')])"))
	public List<WebElement> vndrNMs;
	
	@FindAll(@FindBy(how=How.XPATH,using="//a[contains(text(),'Zum Angebot')]/ancestor::div[5]//div[contains(@class,'position-relative large mb-2')]/strong"))
	public List<WebElement> billingVal;
	
}
