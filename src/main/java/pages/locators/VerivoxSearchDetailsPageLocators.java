package pages.locators;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class VerivoxSearchDetailsPageLocators {

	@FindAll(@FindBy(how=How.XPATH,using="(//div[@class='price'])"))
	public List<WebElement> price;
	
	@FindAll(@FindBy(how=How.XPATH,using="(//a[@class='responsive-label-txt offer-page-cta'])[1]"))
	public WebElement onlineWechseln;
	
	@FindAll(@FindBy(how=How.XPATH,using="(//a[@class='responsive-label-txt offer-page-cta'])[2]"))
	public WebElement jetxtWechseln;
	
	@FindBy(how=How.XPATH,using="//h3[@class='group-header']")
	public WebElement vndrNm_detail;
	
	@FindBy(how=How.XPATH,using="//strong[@data-description='providerName']")
	public WebElement providerNm_detail;
	
	@FindBy(how=How.XPATH,using="//table[@class='costs-table']/tbody[1]/tr[12]/td[2]")
	public WebElement priceInTable;
	
	@FindBy(how=How.XPATH,using="(//span[@class='back'])[1]")
	public WebElement zuruckBtn;
	
	
}
