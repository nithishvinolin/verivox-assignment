@Verivox-Search-DSL
Feature: Acceptance testing to validate if user is able to verify Offer detail page for atleast 5 tariffs
In order to verify,
in Result List page, user click on ZUM ANGEBOT button corresponding to each tariff and validate the necessary information in Tariff details page

  @Verify-offer-detail-page
  Scenario: Validate offer detail
    Given the user is on "https://www.verivox.de/" of Verivox Website
    When he is on "DSL" Calculator
    And he enters prefix/code Ihre Vorwahl as "030" 
    And he select 16 Mbits/s bandwidth selection
    And clicks on the button labeled as JETZT VERGLEICHEN
   	And clicks on ZUM ANGEBOT button on one of the listed Tariffs
	Then he should be able see the details of the selected Tariff
	And he should also see a button labeled as In 5 Minuten online wechseln
