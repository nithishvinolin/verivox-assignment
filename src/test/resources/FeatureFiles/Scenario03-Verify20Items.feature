@Verivox-Search-DSL
Feature: Acceptance testing to validate if user is able to verify pagination for loading the Result List
In order to verify, 
user click on "20 WEITERE TARIFE LADEN" button and list is appended with next 20 and so on until all tariffs are loaded

  @Verify-pagination
  Scenario: Validate DSL Results page
    Given the user is on "https://www.verivox.de/" of Verivox Website
    When he is on "DSL" Calculator
    And he enters prefix/code Ihre Vorwahl as "030" 
    And he select 16 Mbits/s bandwidth selection
    And clicks on the button labeled as JETZT VERGLEICHEN
	And there are more than 20 tariffs available for the provided Vorwahl and bandwidth
	Then User should see button labeled as "20 WEITERE TARIFE LADEN"
	And the list should be appended with next 20 tariffs and so on until all Tariffs are loaded

    