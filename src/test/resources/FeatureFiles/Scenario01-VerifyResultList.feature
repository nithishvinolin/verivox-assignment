@Verivox-Search-DSL
Feature: Acceptance testing to validate if user is able to see the result list page with all available tariffs
In order to verify, 
user search for DSL code and respective area DSL deals load in the Result List page

  @Verify-Results-List
  Scenario: Validate DSL Results page
    Given the user is on "https://www.verivox.de/" of Verivox Website
    When he is on "DSL" Calculator
    And he enters prefix/code Ihre Vorwahl as "030" 
    And he select 16 Mbits/s bandwidth selection
    And clicks on the button labeled as JETZT VERGLEICHEN
	Then he should be able see the Result List page with all the available Tariffs
    And the page title should be "DSL-Preisvergleich aller DSL-Anbieter in Deutschland"

    