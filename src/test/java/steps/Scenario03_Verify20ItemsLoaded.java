package steps;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import cucumber.api.java.en.And;
//import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.actions.VerivoxHomePageActions;
import pages.actions.VerivoxSearchPageActions;
import pages.locators.VerivoxSearchPageLocators;
import utils.SeleniumDriver;


public class Scenario03_Verify20ItemsLoaded {
	public WebDriverWait wait;
	int total;
	int countI=0;
    int countM;
	VerivoxHomePageActions verivoxHomePageActions = new VerivoxHomePageActions();
	VerivoxSearchPageActions verivoxSearchPageActions= new VerivoxSearchPageActions();
	VerivoxSearchPageLocators verivoxSearchPageLocators = new VerivoxSearchPageLocators();
	
	@When("^there are more than 20 tariffs available for the provided Vorwahl and bandwidth$")
	public void verify20Results() throws Throwable {
		if(verivoxSearchPageActions.getExpectedCount()>20) {
			System.out.println("More than 20 tariffs available in the search result");
		}
		else {
			System.out.println("Less than 20 tariffs available in the search result");
		}
		
		boolean buttonExist = verivoxSearchPageActions.weitere_tarife_laden_Exists();
		Assert.assertTrue(verivoxSearchPageActions.getExpectedCount()>20 && buttonExist, "Button does not exist if total result is more than 20 count");
	    if(verivoxSearchPageActions.getExpectedCount()>20 && buttonExist) {
	    	System.out.println("Button Exists in the page if total search result is more than 20");
	    }
		
		
	    
	}

	@Then("^User should see button labeled as \"([^\"]*)\"$")
	public void user_should_see_button_labeled_as(String buttonText) throws Throwable {
		verivoxSearchPageActions.buttonCheck(buttonText);
	    
	    
	}

	@When("^he/she clicks on this button$")
	public void he_she_clicks_on_this_button() throws Throwable {
		verivoxSearchPageActions.click_WEITERE_TARIFE_LADEN();
	    
	    
	}

	@Then("^the list should be appended with next (\\d+) tariffs and so on until all Tariffs are loaded$")
	public void the_list_should_be_appended_with_next_tariffs_and_so_on_until_all_Tariffs_are_loaded(int arg1) throws Throwable {
		total = verivoxSearchPageActions.getCount();
        try {
			while (verivoxSearchPageActions.getExpectedCount()/total>=1) {
				if(total==verivoxSearchPageActions.getExpectedCount()) {
					//total = verivoxSearchPageActions.getCount();
					System.out.println(countM+ " items loaded as expected");
				}
				else {
					countI=total-countI;
					Assert.assertEquals(countI, 20);
					System.out.println(countI+" items loaded as expected");
				}
				   
				verivoxSearchPageActions.click_WEITERE_TARIFE_LADEN();
				   SeleniumDriver.waitForPageToLoad();
				   countM=verivoxSearchPageActions.getExpectedCount()%total;
				   total = verivoxSearchPageActions.getCount();
			   }
		} catch (Exception e) {
			
			Assert.assertEquals(total, verivoxSearchPageActions.getExpectedCount());
			if(total==verivoxSearchPageActions.getExpectedCount()) {
				System.out.println("PASSED - Actual count "+total+" Matched the expected count "+verivoxSearchPageActions.getExpectedCount());
			}
			else {
					System.out.println("FAILED - Actual count "+total+" DID NOT Matched the expected count "+verivoxSearchPageActions.getExpectedCount());
				}
		}
	    
	}
	 


	


}
