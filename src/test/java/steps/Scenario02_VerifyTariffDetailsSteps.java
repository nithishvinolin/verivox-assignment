package steps;

import java.util.List;

import org.testng.Assert;

import cucumber.api.java.en.And;
//import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.actions.VerivoxDetailsPageActions;
import pages.actions.VerivoxHomePageActions;
import pages.actions.VerivoxSearchPageActions;
import pages.locators.VerivoxSearchPageLocators;
import utils.SeleniumDriver;


public class Scenario02_VerifyTariffDetailsSteps {
	
	
	VerivoxHomePageActions verivoxHomePageActions = new VerivoxHomePageActions();
	VerivoxSearchPageActions verivoxSearchPageActions= new VerivoxSearchPageActions();
	VerivoxSearchPageLocators verivoxSearchPageLocators = new VerivoxSearchPageLocators();
	VerivoxDetailsPageActions verivoxDetailsPageActions= new VerivoxDetailsPageActions();
	
	@When("^clicks on ZUM ANGEBOT button on one of the listed Tariffs$")
	@Then("^he should be able see the details of the selected Tariff$")
	@And("^he should also see a button labeled as In 5 Minuten online wechseln$")
	public void verify_Tariff_details() throws Throwable {
	    //for(int i=3;i<7;i++) 
		int i=3;
	    while(i<5)
	    {
	    	verivoxSearchPageActions.getTariffDetails(i);
	    	verivoxSearchPageActions.click_ZUM_ANGEBOT(i);
	    	try {
				verivoxDetailsPageActions.verifyPrice(verivoxSearchPageActions.billingValText);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	try {
				verivoxDetailsPageActions.verifyProviderNM(verivoxSearchPageActions.providerNmText);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	try {
				verivoxDetailsPageActions.verifyVndrNM(verivoxSearchPageActions.vndrNmText);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	try {
				verivoxDetailsPageActions.verifyJetxtWechseln();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	try {
				verivoxDetailsPageActions.verify5MinOnlineWeschseln();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	verivoxDetailsPageActions.click_zurickButton();
	    	
	    	i++;
	    }
	    
	}

	 


	


}
