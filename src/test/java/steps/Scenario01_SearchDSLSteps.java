package steps;

import java.util.List;

import org.testng.Assert;

import cucumber.api.java.en.And;
//import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.actions.VerivoxHomePageActions;
import pages.actions.VerivoxSearchPageActions;
import pages.locators.VerivoxSearchPageLocators;
import utils.SeleniumDriver;


public class Scenario01_SearchDSLSteps {
	
	
	VerivoxHomePageActions verivoxHomePageActions = new VerivoxHomePageActions();
	VerivoxSearchPageActions verivoxSearchPageActions= new VerivoxSearchPageActions();
	VerivoxSearchPageLocators verivoxSearchPageLocators = new VerivoxSearchPageLocators();
	
	@Given("^the user is on \"([^\"]*)\" of Verivox Website$")
	public void i_am_on_the_Home_Page_of_CarsGuide_Website(String webSiteURL)  {
	    SeleniumDriver.openPage(webSiteURL);
	    verivoxHomePageActions.clickPrivacy();
	    
	}

	@When("^he is on \"([^\"]*)\" Calculator$")
	public void i_click_Menu(String arg1) {
		
		verivoxHomePageActions.clickDSL();
		  
	}
	
	@When("^he enters prefix/code Ihre Vorwahl as \"([^\"]*)\"$")
	public void he_enters_prefix_code_Ihre_Vorwahl_as(String vorwahl) throws InterruptedException {
		
		verivoxHomePageActions.enterValueToIhreVorwahl(vorwahl);
	}

	@When("^he select 16 Mbits/s bandwidth selection$")
	public void he_select_bandwidth_selection() {
		verivoxHomePageActions.selectGeschwindigkeit();
	    
	}

	@When("^clicks on the button labeled as JETZT VERGLEICHEN$")
	public void clicks_on_the_button_labeled_as_JETZT_VERGLEICHEN()  {
		verivoxHomePageActions.clickJetztVergleichen();
	}

	
	  @Then("^he should be able see the Result List page with all the available Tariffs$") 
	  public void Result_List_page_with_all_the_available_Tariffs() {
		  verivoxSearchPageActions.tarifBUttonCheck();
	  }
	  
	  @And("^the page title should be \"([^\"]*)\"$") public void
	  the_page_title_should_be(String expectedPageTitle) {
	  
		  verivoxSearchPageActions.verifyTitle(expectedPageTitle);
	  }
	 


	


}
